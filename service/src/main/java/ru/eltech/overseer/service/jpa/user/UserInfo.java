package ru.eltech.overseer.service.jpa.user;

import ru.eltech.overseer.service.rest.ClientProcessSnapshot;

public class UserInfo {

    private User user;

    private ClientProcessSnapshot lastSnapshot;

    public UserInfo(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public ClientProcessSnapshot getLastSnapshot() {
        return lastSnapshot;
    }

    public void setLastSnapshot(ClientProcessSnapshot lastSnapshot) {
        this.lastSnapshot = lastSnapshot;
    }
}
