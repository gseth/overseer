package ru.eltech.overseer.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.eltech.overseer.service.jpa.settings.ClientParamsService;
import ru.eltech.overseer.service.jpa.user.UserInfo;
import ru.eltech.overseer.service.jpa.user.UserService;
import ru.eltech.overseer.service.rest.ClientProcessSnapshot;

import javax.annotation.PreDestroy;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@Component
public class ClientMonitorRunner implements CommandLineRunner {

    private UserService userService;

    private long launchIntervalMillis;

    private ScheduledFuture schedulerHook;

    public ClientMonitorRunner(ClientParamsService paramsService, UserService userService, @Value("${settings.service.clientMonitorPollingInterval.aboveWhispering}") long whisperingIntervalAddendumMillis) {
        this.userService = userService;
        this.launchIntervalMillis = paramsService.get().getWhisperingIntervalMillis() + whisperingIntervalAddendumMillis;
    }

    @Override
    public void run(String... args) throws Exception {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        schedulerHook = executor.scheduleWithFixedDelay(() -> {
            ZonedDateTime timestamp = ZonedDateTime.now();
            List<UserInfo> userInfoList = userService.getAllInfos();
            userInfoList.forEach(userInfo -> {
                ClientProcessSnapshot lastSnapshot = userInfo.getLastSnapshot();
                if (lastSnapshot == null || ChronoUnit.MILLIS.between(lastSnapshot.getDateTime(), timestamp) > launchIntervalMillis) {
                    userService.deactivate(userInfo.getUser());
                }
            });
        }, 0, launchIntervalMillis, TimeUnit.MILLISECONDS);
    }

    @PreDestroy
    public void shutdown() {
        if (schedulerHook != null) {
            schedulerHook.cancel(false);
        }
    }
}
