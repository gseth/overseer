package ru.eltech.overseer.service.jpa.settings;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientParamsRepository extends CrudRepository<ClientParams, Long> {
}
