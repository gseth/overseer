package ru.eltech.overseer.service.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.eltech.overseer.service.jpa.settings.ClientParams;
import ru.eltech.overseer.service.jpa.settings.ClientParamsService;
import ru.eltech.overseer.service.jpa.user.User;
import ru.eltech.overseer.service.jpa.user.UserService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/client")
public class ClientEndpoint {

    private UserService userService;

    private ClientParamsService paramsService;

    @Autowired
    public ClientEndpoint(UserService userService, ClientParamsService paramsService) {
        this.userService = userService;
        this.paramsService = paramsService;
    }

    @PostMapping("/startup")
    public ResponseEntity activateUser(@RequestBody User client) {
        userService.activate(client);
        return ResponseEntity.ok().body(new ClientResponseWrapper(paramsService.get()));
    }

    @PostMapping("/processes")
    public ResponseEntity postCurrentProcesses(@RequestBody ClientSnapshot snapshot) {
        if (!userService.existsByUuid(snapshot.getUuid())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        User user = userService.getByUuid(snapshot.getUuid());
        if (!user.isActive()) {
            userService.activate(user);
        }

        filterDuplicateProcesses(snapshot.getProcessSnapshot());
        userService.saveSnapshot(snapshot);
        return ResponseEntity.ok().build();
    }

    private void filterDuplicateProcesses(ClientProcessSnapshot processSnapshot) {
        TreeSet<ClientProcess> appropriate = processSnapshot.getAppropriateProcesses().stream()
                .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(ClientProcess::getName))));
        processSnapshot.setAppropriateProcesses(new ArrayList<>(appropriate));

        TreeSet<ClientProcess> inappropriate = processSnapshot.getInappropriateProcesses().stream()
                .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(ClientProcess::getName))));
        processSnapshot.setInappropriateProcesses(new ArrayList<>(inappropriate));
    }

    @GetMapping("/params")
    public ResponseEntity<ClientResponseWrapper> getClientParams(@RequestParam("hash") int paramsHash) {
        ClientParams currentParams = paramsService.get();
        if (currentParams.hashCode() != paramsHash) {
            return ResponseEntity.ok().body(new ClientResponseWrapper(currentParams));
        }

        return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
    }

    @PostMapping("/shutdown")
    public ResponseEntity deactivateUser(@RequestBody User client) {
        if (!userService.existsByUuid(client.getUuid())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        userService.deactivate(client);
        return ResponseEntity.ok().build();
    }
}
