package ru.eltech.overseer.service.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.eltech.overseer.service.jpa.settings.ClientParams;
import ru.eltech.overseer.service.jpa.settings.ClientParamsService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin/settings")
public class SettingsController {

    private static final String PATTERN_TYPE_INAPPROPRIATE = "inappropriate";

    private ClientParamsService clientParamsService;

    @Autowired
    public SettingsController(ClientParamsService clientParamsService) {
        this.clientParamsService = clientParamsService;
    }

    @GetMapping
    public String overview(BindingAwareModelMap model) {
        if (!model.containsAttribute("params")) {
            ClientParams clientParams = clientParamsService.get();
            model.addAttribute("params", clientParams);
        }
        return "settings";
    }

    @PostMapping("/general/update")
    public String updateGeneralSettings(@Valid @ModelAttribute("params") ClientParams params, BindingResult binding, RedirectAttributes redirectAttributes) {
        if (binding.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.params", binding);
            redirectAttributes.addFlashAttribute("params", params);
        }
        return "redirect:/admin/settings";
    }

    @PostMapping("/pattern/update")
    public String updatePattern(@RequestParam("type") String patternType, @RequestParam("originalPattern") String originalPattern,
                                @RequestParam("pattern") String pattern, RedirectAttributes redirectAttributes) {

        if (pattern.isEmpty()) {
            redirectAttributes.addFlashAttribute(patternType + "ErroneousPattern", originalPattern);
            redirectAttributes.addFlashAttribute(patternType + "PatternError", "Empty patterns are not allowed");
        } else if (!pattern.equals(originalPattern)) {
            ClientParams params = clientParamsService.get();
            List<String> patterns = patternType.equals(PATTERN_TYPE_INAPPROPRIATE) ? params.getInappropriatePatterns() : params.getAppropriatePatterns();

            if (patterns.contains(pattern)) {
                redirectAttributes.addFlashAttribute(patternType + "ErroneousPattern", originalPattern);
                redirectAttributes.addFlashAttribute(patternType + "PatternError", "Pattern already exists");
            } else {
                if (!patterns.remove(originalPattern)) {
                    return "404";
                }
                patterns.add(pattern);
                clientParamsService.save(params);
            }
        }
        return "redirect:/admin/settings";
    }

    @PostMapping("/pattern/remove")
    public String removePattern(@RequestParam("type") String patternType, @RequestParam("pattern") String pattern) {
        ClientParams params = clientParamsService.get();
        List<String> patterns = patternType.equals(PATTERN_TYPE_INAPPROPRIATE) ? params.getInappropriatePatterns() : params.getAppropriatePatterns();
        patterns.remove(pattern);
        clientParamsService.save(params);
        return "redirect:/admin/settings";
    }

    @PostMapping("/pattern/add")
    public String addPattern(@RequestParam("type") String patternType, @RequestParam("pattern") String pattern, RedirectAttributes redirectAttributes) {
        if (pattern.isEmpty()) {
            redirectAttributes.addFlashAttribute(patternType + "NewPatternError", "Empty patterns are not allowed");
        } else {
            ClientParams params = clientParamsService.get();
            List<String> patterns = patternType.equals(PATTERN_TYPE_INAPPROPRIATE) ? params.getInappropriatePatterns() : params.getAppropriatePatterns();
            if (patterns.contains(pattern)) {
                redirectAttributes.addFlashAttribute(patternType + "NewErroneousPattern", pattern);
                redirectAttributes.addFlashAttribute(patternType + "NewPatternError", "Pattern already exists");
            } else {
                patterns.add(pattern);
                clientParamsService.save(params);
            }
        }
        return "redirect:/admin/settings";
    }
}
