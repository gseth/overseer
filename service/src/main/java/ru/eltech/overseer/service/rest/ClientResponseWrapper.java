package ru.eltech.overseer.service.rest;

import ru.eltech.overseer.service.jpa.settings.ClientParams;

public class ClientResponseWrapper {

    private ClientParams whisperingParams;

    private String error;

    public ClientResponseWrapper(ClientParams whisperingParams) {
        this.whisperingParams = whisperingParams;
    }

    public ClientResponseWrapper(String error) {
        this.error = error;
    }

    public ClientParams getWhisperingParams() {
        return whisperingParams;
    }

    public String getError() {
        return error;
    }
}
