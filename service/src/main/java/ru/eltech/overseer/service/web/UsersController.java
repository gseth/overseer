package ru.eltech.overseer.service.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.eltech.overseer.service.jpa.user.User;
import ru.eltech.overseer.service.jpa.user.UserInfo;
import ru.eltech.overseer.service.jpa.user.UserService;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin/users")
public class UsersController {

    private static final int USERNAME_SIZE = 30;

    private static final Set<String> SUPPORTED_AVATAR_EXTENSIONS = new HashSet<>(Arrays.asList("jpeg", "jpg", "png"));

    private UserService userService;

    @Autowired
    public UsersController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String list(Model model) {
        List<UserInfoWrapper> infoWrappers = userService.getAllInfos().stream().map(this::wrap).collect(Collectors.toList());
        model.addAttribute("userInfos", infoWrappers);
        return "users";
    }

    @GetMapping("/{uuid}")
    public String user(@PathVariable("uuid") String uuid, Model model) {
        if (!userService.existsByUuid(uuid)) {
            return "error/404";
        }

        model.addAttribute("info", wrap(userService.getInfoByUuid(uuid)));
        return "user-page";
    }

    @PostMapping("/{uuid}/delete")
    public String removeUser(@PathVariable("uuid") String uuid) {
        userService.removeByUuid(uuid);
        return "redirect:/admin/users";
    }

    @PostMapping("/{uuid}/name")
    public String updateUsername(@PathVariable("uuid") String uuid, @RequestParam("username") String username, RedirectAttributes redirectAttributes) {
        username = username.trim();
        if (username.isEmpty() || username.length() > USERNAME_SIZE) {
            redirectAttributes.addFlashAttribute("usernameError", "User name must not be longer than " + USERNAME_SIZE + " characters");
        } else {
            User user = userService.getByUuid(uuid);
            user.setUsername(username);
            userService.update(user);
        }

        return "redirect:/admin/users/" + uuid;
    }

    @PostMapping("/{uuid}/avatar")
    public String uploadBookFile(@PathVariable("uuid") String uuid, @RequestParam("avatar") MultipartFile avatarFile, RedirectAttributes redirectAttributes) {
        String[] splittedFilename = avatarFile.getOriginalFilename().split("\\.");
        if (splittedFilename.length < 2) {
            redirectAttributes.addFlashAttribute("avatarError", "Unable to process the file");
            return "redirect:/admin/users/" + uuid;
        }

        String supposedExtension = splittedFilename[splittedFilename.length - 1].toLowerCase();
        if (!SUPPORTED_AVATAR_EXTENSIONS.contains(supposedExtension)) {
            redirectAttributes.addFlashAttribute("avatarError", "Extension '" + supposedExtension + "' is not supported");
            return "redirect:/admin/users/" + uuid;
        }

        try {
            User user = userService.getByUuid(uuid);
            user.setUserpic(avatarFile.getBytes());
            userService.update(user);
        } catch (IOException e) {
            return "error/404";
        }

        return "redirect:/admin/users/" + uuid;
    }

    private UserInfoWrapper wrap(UserInfo info) {
        byte[] userpicBytes = info.getUser().getUserpic();
        String userpic = userpicBytes != null ? "data:image/png;base64," + Base64.getEncoder().encodeToString(userpicBytes) : null;
        return new UserInfoWrapper(info, userpic);
    }

    private class UserInfoWrapper {

        private UserInfo userInfo;

        private String userpic;

        public UserInfoWrapper(UserInfo userInfo, String userpic) {
            this.userInfo = userInfo;
            this.userpic = userpic;
        }

        public UserInfo getUserInfo() {
            return userInfo;
        }

        public String getUserpic() {
            return userpic;
        }
    }
}
