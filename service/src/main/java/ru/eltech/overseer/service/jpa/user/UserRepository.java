package ru.eltech.overseer.service.jpa.user;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {
    Iterable<User> findAllByOrderByActiveDescUsernameAscComputerNameAsc();
}
