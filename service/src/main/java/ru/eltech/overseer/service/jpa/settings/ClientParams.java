package ru.eltech.overseer.service.jpa.settings;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Access(AccessType.FIELD)
public class ClientParams {

    @Id
    @GeneratedValue
    private long id;

    @NotNull(message = "{message.param.notnull}")
    @Positive(message = "{message.param.positive}")
    @Max(value = Integer.MAX_VALUE, message = "{message.param.max}")
    @Column
    private Long whisperingIntervalMillis;

    @NotNull(message = "{message.param.notnull}")
    @Positive(message = "{message.param.positive}")
    @Max(value = Integer.MAX_VALUE, message = "{message.param.max}")
    @Column
    private Long paramsPollingIntervalMillis;

    @ElementCollection//(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> appropriatePatterns = new ArrayList<>();

    @ElementCollection//(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> inappropriatePatterns = new ArrayList<>();


    protected long getId() {
        return id;
    }

    protected void setId(long id) {
        this.id = id;
    }

    public Long getWhisperingIntervalMillis() {
        return whisperingIntervalMillis;
    }

    public void setWhisperingIntervalMillis(Long whisperingIntervalMillis) {
        this.whisperingIntervalMillis = whisperingIntervalMillis;
    }

    public Long getParamsPollingIntervalMillis() {
        return paramsPollingIntervalMillis;
    }

    public void setParamsPollingIntervalMillis(Long paramsPollingIntervalMillis) {
        this.paramsPollingIntervalMillis = paramsPollingIntervalMillis;
    }

    public List<String> getAppropriatePatterns() {
        return appropriatePatterns;
    }

    public void setAppropriatePatterns(List<String> appropriatePatterns) {
        this.appropriatePatterns = appropriatePatterns;
    }

    public List<String> getInappropriatePatterns() {
        return inappropriatePatterns;
    }

    public void setInappropriatePatterns(List<String> inappropriatePatterns) {
        this.inappropriatePatterns = inappropriatePatterns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientParams that = (ClientParams) o;
        return Objects.equals(whisperingIntervalMillis, that.whisperingIntervalMillis) &&
                Objects.equals(paramsPollingIntervalMillis, that.paramsPollingIntervalMillis) &&
                Objects.equals(appropriatePatterns, that.appropriatePatterns) &&
                Objects.equals(inappropriatePatterns, that.inappropriatePatterns);
    }

    @Override
    public int hashCode() {
        return Objects.hash(whisperingIntervalMillis, paramsPollingIntervalMillis, appropriatePatterns, inappropriatePatterns);
    }
}
