package ru.eltech.overseer.service.jpa.user;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProcessSnapshotRepository extends CrudRepository<ProcessSnapshot, Long> {
    Optional<ProcessSnapshot> findFirstByUserUuidOrderByDateTimeDesc(String uuid);
}
