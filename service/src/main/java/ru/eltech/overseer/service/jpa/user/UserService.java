package ru.eltech.overseer.service.jpa.user;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.eltech.overseer.service.rest.ClientProcess;
import ru.eltech.overseer.service.rest.ClientProcessSnapshot;
import ru.eltech.overseer.service.rest.ClientSnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;

    private ProcessSnapshotRepository snapshotRepository;

    private Gson gson;

    @Autowired
    public UserService(UserRepository userRepository, ProcessSnapshotRepository snapshotRepository, Gson gson) {
        this.userRepository = userRepository;
        this.snapshotRepository = snapshotRepository;
        this.gson = gson;
    }

    public User activate(User user) {
        user = mergeForActivation(user);
        user.setActive(true);
        return userRepository.save(user);
    }

    public User deactivate(User user) {
        user = mergeForActivation(user);
        user.setActive(false);
        return userRepository.save(user);
    }

    public User update(User user) {
        user = mergeForUpdate(user);
        return userRepository.save(user);
    }

    public void removeByUuid(String uuid) {
        userRepository.deleteById(uuid);
    }

    private User mergeForActivation(User another) {
        Optional<User> byId = userRepository.findById(another.getUuid());
        byId.ifPresent(user -> {
            if (another.getComputerName() != null) {
                user.setComputerName(another.getComputerName());
            }
        });

        return byId.orElse(another);
    }

    private User mergeForUpdate(User another) {
        Optional<User> byId = userRepository.findById(another.getUuid());
        byId.ifPresent(user -> {
            if (another.getUsername() != null) {
                user.setUsername(another.getUsername());
            }
            if (another.getUserpic() != null) {
                user.setUserpic(another.getUserpic());
            }
        });

        return byId.orElseThrow(() -> new UserNotFoundException(another.getUuid()));
    }

    public boolean existsByUuid(String uuid) {
        return userRepository.existsById(uuid);
    }

    public User getByUuid(String uuid) {
        return userRepository.findById(uuid).orElseThrow(() -> new UserNotFoundException(uuid));
    }

    public void saveSnapshot(ClientSnapshot clientSnapshot) {
        ProcessSnapshot processSnapshot = new ProcessSnapshot();
        processSnapshot.setUser(userRepository.findById(clientSnapshot.getUuid()).orElseThrow(() -> new UserNotFoundException(clientSnapshot.getUuid())));

        ClientProcessSnapshot clientProcessSnapshot = clientSnapshot.getProcessSnapshot();
        processSnapshot.setAppropriateProcesses(gson.toJson(clientProcessSnapshot.getAppropriateProcesses()));
        processSnapshot.setInappropriateProcesses(gson.toJson(clientProcessSnapshot.getInappropriateProcesses()));
        processSnapshot.setDateTime(clientProcessSnapshot.getDateTime());

        snapshotRepository.save(processSnapshot);
    }

    public List<UserInfo> getAllInfos() {
        Iterable<User> allUsers = userRepository.findAllByOrderByActiveDescUsernameAscComputerNameAsc();
        List<UserInfo> infos = new ArrayList<>();
        allUsers.forEach(user -> {
            UserInfo infoByUuid = getInfoByUuid(user.getUuid());
            infos.add(infoByUuid);
        });
        return infos;
    }

    public UserInfo getInfoByUuid(String uuid) {
        User user = userRepository.findById(uuid).orElseThrow(() -> new UserNotFoundException(uuid));
        UserInfo userInfo = new UserInfo(user);

        Optional<ProcessSnapshot> lastProcessSnapshot = snapshotRepository.findFirstByUserUuidOrderByDateTimeDesc(uuid);
        lastProcessSnapshot.ifPresent(snapshot -> {
            ClientProcessSnapshot clientProcessSnapshot = new ClientProcessSnapshot(snapshot.getDateTime(),
                    convertFromSerializedForm(snapshot.getAppropriateProcesses()),
                    convertFromSerializedForm(snapshot.getInappropriateProcesses()));

            userInfo.setLastSnapshot(clientProcessSnapshot);
        });

        return userInfo;
    }

    private List<ClientProcess> convertFromSerializedForm(String processesJson) {
        return gson.fromJson(processesJson, new TypeToken<List<ClientProcess>>() {
        }.getType());
    }
}
