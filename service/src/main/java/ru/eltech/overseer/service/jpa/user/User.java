package ru.eltech.overseer.service.jpa.user;

import javax.persistence.*;
import java.util.List;

@Entity
@Access(AccessType.FIELD)
public class User {

    @Id
    private String uuid;

    @Column(nullable = false)
    private String computerName;

    @Lob
    @Column
    private byte[] userpic;

    @Column
    private String username;

    @Column(nullable = false)
    private boolean active;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<ProcessSnapshot> snapshots;

    protected User() {
    }

    public User(String uuid, String computerName) {
        this.uuid = uuid;
        this.computerName = computerName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getComputerName() {
        return computerName;
    }

    public void setComputerName(String computerName) {
        this.computerName = computerName;
    }

    public byte[] getUserpic() {
        return userpic;
    }

    public void setUserpic(byte[] userpic) {
        this.userpic = userpic;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<ProcessSnapshot> getSnapshots() {
        return snapshots;
    }

    public void setSnapshots(List<ProcessSnapshot> snapshots) {
        this.snapshots = snapshots;
    }
}
