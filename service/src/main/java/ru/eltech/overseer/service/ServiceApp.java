package ru.eltech.overseer.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ru.eltech.overseer.service.jpa.settings.ClientParams;

@SpringBootApplication
public class ServiceApp implements WebMvcConfigurer {

    @Value("${settings.default.client.whisperingInterval}")
    private long defaultWhisperingIntervalMillis;

    @Value("${settings.default.client.paramsPollingInterval}")
    private long defaultParamsPollingIntervalMillis;

    @Bean
    public Gson gson() {
        return new GsonBuilder().create();
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());
        return bean;
    }

    @Bean
    public ClientParams defaultClientParams() {
        ClientParams clientParams = new ClientParams();
        clientParams.setWhisperingIntervalMillis(defaultWhisperingIntervalMillis);
        clientParams.setParamsPollingIntervalMillis(defaultParamsPollingIntervalMillis);
        return clientParams;
    }

    public static void main(String[] args) {
        SpringApplication.run(ServiceApp.class, args);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry r) {
        r.addViewController("/login").setViewName("login");
        r.addRedirectViewController("/", "/admin/users");
        r.addRedirectViewController("/admin", "/admin/users");
    }

    @Configuration
    @EnableWebSecurity
    public class SecurityConfig extends WebSecurityConfigurerAdapter {

        @Bean
        public PasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder(8);
        }

        @Autowired
        public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
            auth
                    .inMemoryAuthentication()
                    .withUser("admin").password("$2a$06$YUFseW5frvLFP1ROsEBjzOlmmWtCHTS98hPMDhrVknrJndCm/Wcoy").roles("ADMIN");
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .formLogin().loginPage("/login").usernameParameter("login").passwordParameter("password")
                    .and()
                    .authorizeRequests().antMatchers("/login").anonymous()
                    .and()
                    .authorizeRequests().antMatchers("/h2/**").permitAll()
                    .and()
                    .authorizeRequests().antMatchers("/client/startup", "/client/shutdown", "/client/processes", "/client/params").permitAll()
                    .and()
                    .authorizeRequests().anyRequest().hasAnyRole("ADMIN")
                    .and()
                    .httpBasic().disable()
                    .csrf().disable();
        }

        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring().antMatchers("/js/*/**", "/css/*/**", "/fonts/*/**");
        }
    }
}
