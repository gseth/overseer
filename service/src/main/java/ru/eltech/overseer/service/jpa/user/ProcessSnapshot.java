package ru.eltech.overseer.service.jpa.user;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Access(AccessType.FIELD)
public class ProcessSnapshot {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;

    @Column
    private ZonedDateTime dateTime;

    @Lob
    @Column
    private String appropriateProcesses;

    @Lob
    @Column
    private String inappropriateProcesses;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(ZonedDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getAppropriateProcesses() {
        return appropriateProcesses;
    }

    public void setAppropriateProcesses(String appropriateProcesses) {
        this.appropriateProcesses = appropriateProcesses;
    }

    public String getInappropriateProcesses() {
        return inappropriateProcesses;
    }

    public void setInappropriateProcesses(String inappropriateProcesses) {
        this.inappropriateProcesses = inappropriateProcesses;
    }
}
