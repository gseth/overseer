package ru.eltech.overseer.service.rest;

import com.fasterxml.jackson.annotation.JsonAlias;

public class ClientSnapshot {

    private String uuid;

    @JsonAlias("snapshot")
    private ClientProcessSnapshot processSnapshot;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ClientProcessSnapshot getProcessSnapshot() {
        return processSnapshot;
    }

    public void setProcessSnapshot(ClientProcessSnapshot processSnapshot) {
        this.processSnapshot = processSnapshot;
    }
}
