package ru.eltech.overseer.service.jpa.settings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Iterator;

@Service
public class ClientParamsService {

    private ClientParamsRepository repository;

    private ClientParams defaultParams;

    @Autowired
    public ClientParamsService(ClientParamsRepository repository, ClientParams defaultParams) {
        this.repository = repository;
        this.defaultParams = defaultParams;
    }

    public ClientParams get() {
        Iterator<ClientParams> paramsItr = repository.findAll().iterator();
        if (paramsItr.hasNext()) {
            return paramsItr.next();
        }
        return null;
    }

    public ClientParams save(ClientParams params) {
        repository.deleteAll();
        params.getInappropriatePatterns().sort(String::compareToIgnoreCase);
        params.getAppropriatePatterns().sort(String::compareToIgnoreCase);
        return repository.save(params);
    }

    @PostConstruct
    public void init() {
        if (get() == null) {
            save(defaultParams);
        }
    }
}
