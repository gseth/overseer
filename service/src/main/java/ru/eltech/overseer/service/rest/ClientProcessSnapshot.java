package ru.eltech.overseer.service.rest;

import java.time.ZonedDateTime;
import java.util.List;

public class ClientProcessSnapshot {

    private ZonedDateTime dateTime;

    private List<ClientProcess> appropriateProcesses;

    private List<ClientProcess> inappropriateProcesses;

    public ClientProcessSnapshot() {
    }

    public ClientProcessSnapshot(ZonedDateTime dateTime, List<ClientProcess> appropriateProcesses, List<ClientProcess> inappropriateProcesses) {
        this.dateTime = dateTime;
        this.appropriateProcesses = appropriateProcesses;
        this.inappropriateProcesses = inappropriateProcesses;
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(ZonedDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public List<ClientProcess> getAppropriateProcesses() {
        return appropriateProcesses;
    }

    public void setAppropriateProcesses(List<ClientProcess> appropriateProcesses) {
        this.appropriateProcesses = appropriateProcesses;
    }

    public List<ClientProcess> getInappropriateProcesses() {
        return inappropriateProcesses;
    }

    public void setInappropriateProcesses(List<ClientProcess> inappropriateProcesses) {
        this.inappropriateProcesses = inappropriateProcesses;
    }
}
