package ru.eltech.overseer.service.jpa.user;

public class UserNotFoundException extends IllegalArgumentException {

    private String uuid;

    public UserNotFoundException(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
