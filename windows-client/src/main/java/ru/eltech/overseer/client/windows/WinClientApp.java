package ru.eltech.overseer.client.windows;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class WinClientApp {

    @Value("${service.client.url}")
    private String clientEndpointUrl;

    @Value("${service.timeout.connect}")
    private int connectTimeout;

    @Bean
    public RestTemplate serviceRestTemplate() {
        return new RestTemplateBuilder()
                .setConnectTimeout(connectTimeout)
                .rootUri(clientEndpointUrl)
                .build();
    }

    @Bean
    public Unmarshaller processListUnmarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan("ru.eltech.overseer.client.windows.os.wmic");
        return marshaller;
    }

    public static void main(String[] args) {
        SpringApplication.run(WinClientApp.class, args);
    }
}
