package ru.eltech.overseer.client.windows.os;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class CommandExecutor {

    public <T> T executeWithOutput(String cmd, Function<String, T> outputConverter) throws CommandExecutionException {
        return outputConverter.apply(executeWithOutput(cmd));
    }

    public String executeWithOutput(String cmd) {
        BufferedReader stdInput;
        BufferedReader stdError;
        try {
            Process process = Runtime.getRuntime().exec(cmd);
            stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            String result = accumulateOutput(stdInput);
            if (!result.isEmpty()) {
                return result;
            }

            String err = accumulateOutput(stdError);
            if (!err.isEmpty()) {
                throw new CommandExecutionException(err);
            }

            return result;
        } catch (IOException e) {
            throw new CommandExecutionException(e);
        }
    }

    private String accumulateOutput(BufferedReader reader) throws CommandExecutionException {
        try {
            return String.join("\n", reader.lines().collect(Collectors.toList()));
        } catch (UncheckedIOException e) {
            throw new CommandExecutionException(e);
        }
    }
}
