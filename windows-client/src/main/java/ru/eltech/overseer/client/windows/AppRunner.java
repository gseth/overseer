package ru.eltech.overseer.client.windows;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.eltech.overseer.client.windows.processes.ProcessWhisperer;
import ru.eltech.overseer.client.windows.rest.RepeatedConnector;
import ru.eltech.overseer.client.windows.rest.ServiceConnector;

import javax.annotation.PreDestroy;

@Component
public class AppRunner implements CommandLineRunner {

    private ProcessWhisperer whisperer;

    private ServiceConnector serviceConnector;

    @Autowired
    public AppRunner(ProcessWhisperer whisperer, ServiceConnector serviceConnector) {
        this.whisperer = whisperer;
        this.serviceConnector = serviceConnector;
    }

    @Override
    public void run(String... args) throws Exception {
        RepeatedConnector.connectUninterruptably(() -> serviceConnector.sendStartup());
        whisperer.start();
    }

    @PreDestroy
    public void shutdown() {
        whisperer.stop();
        serviceConnector.sendShutdown();
    }
}
