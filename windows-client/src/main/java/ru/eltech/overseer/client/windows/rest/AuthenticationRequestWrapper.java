package ru.eltech.overseer.client.windows.rest;

public class AuthenticationRequestWrapper extends ServiceRequestWrapper {

    private String computerName;

    public AuthenticationRequestWrapper(String uuid, String computerName) {
        super(uuid);
        this.computerName = computerName;
    }

    public String getComputerName() {
        return computerName;
    }
}
