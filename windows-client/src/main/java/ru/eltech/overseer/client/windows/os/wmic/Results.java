package ru.eltech.overseer.client.windows.os.wmic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RESULTS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Results {

    @XmlElement(name = "CIM", required = true)
    private Model model;

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }
}
