package ru.eltech.overseer.client.windows.os.wmic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "CIM")
@XmlAccessorType(XmlAccessType.FIELD)
public class Model {

    @XmlElement(name = "INSTANCE")
    private List<Instance> processes;

    public List<Instance> getProcesses() {
        return processes;
    }

    public void setProcesses(List<Instance> processes) {
        this.processes = processes;
    }
}
