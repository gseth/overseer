package ru.eltech.overseer.client.windows.rest;

public abstract class ServiceReponseWrapper {

    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
