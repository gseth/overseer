package ru.eltech.overseer.client.windows.processes;

import java.time.ZonedDateTime;
import java.util.List;

public class ProcessSnapshot {

    private ZonedDateTime dateTime;

    private List<Process> appropriateProcesses;

    private List<Process> inappropriateProcesses;

    public ProcessSnapshot(ZonedDateTime dateTime, List<Process> appropriateProcesses, List<Process> inappropriateProcesses) {
        this.dateTime = dateTime;
        this.appropriateProcesses = appropriateProcesses;
        this.inappropriateProcesses = inappropriateProcesses;
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }

    public List<Process> getAppropriateProcesses() {
        return appropriateProcesses;
    }

    public List<Process> getInappropriateProcesses() {
        return inappropriateProcesses;
    }
}
