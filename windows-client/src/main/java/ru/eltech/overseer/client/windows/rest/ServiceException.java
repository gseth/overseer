package ru.eltech.overseer.client.windows.rest;

public class ServiceException extends RuntimeException {

    private int status;

    private String message;

    public ServiceException(int status) {
        this.status = status;
    }

    public ServiceException(int status, String message) {
        super(message);
        this.status = status;
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
