package ru.eltech.overseer.client.windows.rest;

import java.util.concurrent.TimeUnit;

public class RepeatedConnector {

    private static final long INITIAL_FAIL_SLEEP_MINUTES = 0;

    private static final long FAIL_SLEEP_THRESHOLD_MINUTES = 5;

    public static void connectUninterruptably(Runnable func) throws InterruptedException {
        Exception serviceException = null;
        long failSleep = INITIAL_FAIL_SLEEP_MINUTES;
        do {
            serviceException = null;
            try {
                func.run();
            } catch (ServiceException e) {
                e.printStackTrace(System.err);

                serviceException = e;
                failSleep = failSleep < FAIL_SLEEP_THRESHOLD_MINUTES ? ++failSleep : failSleep;
                TimeUnit.MINUTES.sleep(failSleep);
            }
        } while (serviceException != null);
    }
}
