package ru.eltech.overseer.client.windows.rest;

import ru.eltech.overseer.client.windows.processes.ProcessSnapshot;

public class ProcessSnapshotWrapper extends ServiceRequestWrapper {

    private ProcessSnapshot snapshot;

    public ProcessSnapshotWrapper(String uuid, ProcessSnapshot snapshot) {
        super(uuid);
        this.snapshot = snapshot;
    }

    public ProcessSnapshot getSnapshot() {
        return snapshot;
    }
}
