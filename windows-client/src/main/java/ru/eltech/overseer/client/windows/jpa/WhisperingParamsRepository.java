package ru.eltech.overseer.client.windows.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WhisperingParamsRepository extends CrudRepository<WhisperingParams, Long> {
}
