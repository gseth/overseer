package ru.eltech.overseer.client.windows.os;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.Unmarshaller;
import org.springframework.stereotype.Service;
import org.springframework.xml.transform.StringSource;
import ru.eltech.overseer.client.windows.os.wmic.Command;
import ru.eltech.overseer.client.windows.os.wmic.Instance;
import ru.eltech.overseer.client.windows.os.wmic.Property;
import ru.eltech.overseer.client.windows.processes.Process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CommandService {

    private CommandExecutor executor;

    private Unmarshaller processListUnmarshaller;

    private String uuid;

    private String computerName;

    @Autowired
    public CommandService(CommandExecutor executor, Unmarshaller processListUnmarshaller) {
        this.executor = executor;
        this.processListUnmarshaller = processListUnmarshaller;
    }

    public String getUuid() {
        if (uuid == null) {
            uuid = executor.executeWithOutput("wmic csproduct get UUID", rawUuid -> {
                Pattern pattern = Pattern.compile("\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}");
                Matcher matcher = pattern.matcher(rawUuid);
                if (!matcher.find()) {
                    throw new CommandExecutionException(String.format("Cannot parse UUID - %s doesn't match pattern", rawUuid));
                }

                return matcher.group();
            });
        }

        return uuid;
    }

    public String getComputerName() {
        if (computerName == null) {
            computerName = executor.executeWithOutput("hostname");
        }

        return computerName;
    }

    public List<Process> getProcessList() {
        try {
            return getProcessListWithCommand("wmic path win32_process get Caption,Name,Processid,Commandline /format:rawxml");
        } catch (CommandExecutionException e) {
            return getProcessListWithCommand("wmic process list /format:rawxml");
        }
    }

    private List<Process> getProcessListWithCommand(String command) {
        return executor.executeWithOutput(command, processXml -> {
            try {
                Command wmicCommand = (Command) processListUnmarshaller.unmarshal(new StringSource(processXml));
                List<Instance> rawProcesses = wmicCommand.getResults().getModel().getProcesses();
                return convertToProcesses(rawProcesses);
            } catch (IOException e) {
                throw new CommandExecutionException(e);
            }
        });
    }

    private List<Process> convertToProcesses(List<Instance> rawProcesses) {
        List<Process> processList = new ArrayList<>();
        for (Instance instance : rawProcesses) {
            Process process = new Process();
            for (Property property : instance.getProperties()) {
                switch (property.getName()) {
                    case "Caption":
                        process.setCaption(property.getValue());
                        break;
                    case "Name":
                        process.setName(property.getValue());
                        break;
                    case "CommandLine":
                        process.setCommandLine(property.getValue());
                        break;
                    case "ProcessId":
                        process.setId(Integer.valueOf(property.getValue()));
                        break;
                }
            }
            processList.add(process);
        }
        processList.sort(Comparator.comparing(Process::getName));
        return processList;
    }
}
