package ru.eltech.overseer.client.windows.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

@Service
public class WhisperingParamsService {

    private static final WhisperingParams DEFAULT_PARAMS = new WhisperingParams(TimeUnit.MINUTES.toMillis(5), TimeUnit.MINUTES.toMillis(10));

    private WhisperingParamsRepository repository;

    @Autowired
    public WhisperingParamsService(WhisperingParamsRepository repository) {
        this.repository = repository;
    }

    public WhisperingParams get() {
        Iterator<WhisperingParams> paramsItr = repository.findAll().iterator();
        if (paramsItr.hasNext()) {
            return paramsItr.next();
        }
        return null;
    }

    public WhisperingParams save(WhisperingParams params) {
        repository.deleteAll();
        return repository.save(params);
    }

    @PostConstruct
    public void init() {
        if (get() == null) {
            save(DEFAULT_PARAMS);
        }
    }
}
