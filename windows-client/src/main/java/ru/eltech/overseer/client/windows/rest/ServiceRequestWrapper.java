package ru.eltech.overseer.client.windows.rest;

public abstract class ServiceRequestWrapper {

    private String uuid;

    public ServiceRequestWrapper(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
