package ru.eltech.overseer.client.windows.os.wmic;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "PROPERTY")
@XmlAccessorType(XmlAccessType.FIELD)
public class Property {

    @XmlAttribute(name = "NAME")
    private String name;

    @XmlElement(name = "VALUE")
    private String value;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
