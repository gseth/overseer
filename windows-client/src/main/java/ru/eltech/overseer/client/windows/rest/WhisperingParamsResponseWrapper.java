package ru.eltech.overseer.client.windows.rest;

import ru.eltech.overseer.client.windows.jpa.WhisperingParams;

public class WhisperingParamsResponseWrapper extends ServiceReponseWrapper {

    private WhisperingParams whisperingParams;

    public WhisperingParams getWhisperingParams() {
        return whisperingParams;
    }

    public void setWhisperingParams(WhisperingParams whisperingParams) {
        this.whisperingParams = whisperingParams;
    }
}
