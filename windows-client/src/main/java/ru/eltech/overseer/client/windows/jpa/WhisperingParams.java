package ru.eltech.overseer.client.windows.jpa;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Access(AccessType.FIELD)
public class WhisperingParams {

    @Id
    @GeneratedValue
    private long id;

    @Column
    private long whisperingIntervalMillis;

    @Column
    private long paramsPollingIntervalMillis;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> appropriatePatterns;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> inappropriatePatterns;

    public WhisperingParams() {
    }

    public WhisperingParams(long whisperingIntervalMillis, long paramsPollingIntervalMillis) {
        this.whisperingIntervalMillis = whisperingIntervalMillis;
        this.paramsPollingIntervalMillis = paramsPollingIntervalMillis;
        this.appropriatePatterns = new HashSet<>();
        this.inappropriatePatterns = new HashSet<>();
    }

    protected long getId() {
        return id;
    }

    protected void setId(long id) {
        this.id = id;
    }

    public long getWhisperingIntervalMillis() {
        return whisperingIntervalMillis;
    }

    public void setWhisperingIntervalMillis(long whisperingIntervalMillis) {
        this.whisperingIntervalMillis = whisperingIntervalMillis;
    }

    public long getParamsPollingIntervalMillis() {
        return paramsPollingIntervalMillis;
    }

    public void setParamsPollingIntervalMillis(long paramsPollingIntervalMillis) {
        this.paramsPollingIntervalMillis = paramsPollingIntervalMillis;
    }

    public Set<String> getAppropriatePatterns() {
        return appropriatePatterns;
    }

    public void setAppropriatePatterns(Set<String> appropriatePatterns) {
        this.appropriatePatterns = appropriatePatterns;
    }

    public Set<String> getInappropriatePatterns() {
        return inappropriatePatterns;
    }

    public void setInappropriatePatterns(Set<String> inappropriatePatterns) {
        this.inappropriatePatterns = inappropriatePatterns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WhisperingParams that = (WhisperingParams) o;
        return whisperingIntervalMillis == that.whisperingIntervalMillis &&
                paramsPollingIntervalMillis == that.paramsPollingIntervalMillis &&
                Objects.equals(appropriatePatterns, that.appropriatePatterns) &&
                Objects.equals(inappropriatePatterns, that.inappropriatePatterns);
    }

    @Override
    public int hashCode() {
        return Objects.hash(whisperingIntervalMillis, paramsPollingIntervalMillis, appropriatePatterns, inappropriatePatterns);
    }
}
