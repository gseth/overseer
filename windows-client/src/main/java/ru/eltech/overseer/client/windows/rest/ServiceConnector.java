package ru.eltech.overseer.client.windows.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import ru.eltech.overseer.client.windows.jpa.WhisperingParams;
import ru.eltech.overseer.client.windows.os.CommandService;
import ru.eltech.overseer.client.windows.processes.ProcessSnapshot;

import java.util.Optional;

@Component
public class ServiceConnector {

    private CommandService commandService;

    private RestTemplate clientEndpoint;

    @Autowired
    public ServiceConnector(CommandService commandService, RestTemplate clientEndpoint) {
        this.commandService = commandService;
        this.clientEndpoint = clientEndpoint;
    }

    public void sendStartup() {
        post("/startup", new AuthenticationRequestWrapper(commandService.getUuid(), commandService.getComputerName()));
    }

    public void sendProcesses(ProcessSnapshot snapshot) {
        ProcessSnapshotWrapper processSnapshotWrapper = new ProcessSnapshotWrapper(commandService.getUuid(), snapshot);
        post("/processes", processSnapshotWrapper);
    }

    public void sendShutdown() {
        clientEndpoint.postForLocation("/shutdown", new AuthenticationRequestWrapper(commandService.getUuid(), commandService.getComputerName()));
    }

    public Optional<WhisperingParams> obtainParams(WhisperingParams currentParams) {
        int currentParamsHash = currentParams != null ? currentParams.hashCode() : 0;
        ResponseEntity<WhisperingParamsResponseWrapper> response = clientEndpoint.getForEntity("/params?hash=" + currentParamsHash, WhisperingParamsResponseWrapper.class);
        if (response.getStatusCode() != HttpStatus.OK && response.getStatusCode() != HttpStatus.NOT_MODIFIED) {
            if (response.getBody() == null) {
                throw new ServiceException(response.getStatusCodeValue());
            } else {
                throw new ServiceException(response.getStatusCodeValue(), response.getBody().getError());
            }
        }

        if (response.getBody() != null) {
            return Optional.of(response.getBody().getWhisperingParams());
        }

        return Optional.empty();
    }

    private void post(String url, Object requestBody) {
        try {
            ResponseEntity<WhisperingParamsResponseWrapper> response = clientEndpoint.postForEntity(url, requestBody, WhisperingParamsResponseWrapper.class);

            if (response.getStatusCode() != HttpStatus.OK) {
                if (response.getBody() == null) {
                    throw new ServiceException(response.getStatusCodeValue());
                } else {
                    throw new ServiceException(response.getStatusCodeValue(), response.getBody().getError());
                }
            }
        } catch (RestClientException e) {
            throw new ServiceException(e);
        }
    }
}
