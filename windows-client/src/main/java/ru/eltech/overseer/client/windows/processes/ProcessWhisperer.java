package ru.eltech.overseer.client.windows.processes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.eltech.overseer.client.windows.jpa.WhisperingParams;
import ru.eltech.overseer.client.windows.jpa.WhisperingParamsService;
import ru.eltech.overseer.client.windows.os.CommandService;
import ru.eltech.overseer.client.windows.rest.ServiceConnector;
import ru.eltech.overseer.client.windows.rest.ServiceException;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class ProcessWhisperer {

    private static final String PROCESS_PATTERN_TEMPLATE = "^(%s)+$";

    private final WhisperingParamsService paramsService;

    private final ServiceConnector serviceConnector;

    private final CommandService commandService;


    private ScheduledFuture whisperExecutorHook;

    private ScheduledFuture paramsListenerHook;

    private WhisperingParams lastKnownParams;

    private Predicate<String> lastKnownAppropriatePattern;

    private Predicate<String> lastKnownInappropriatePattern;

    @Autowired
    public ProcessWhisperer(WhisperingParamsService paramsService, ServiceConnector serviceConnector, CommandService commandService) {
        this.paramsService = paramsService;
        this.serviceConnector = serviceConnector;
        this.commandService = commandService;
    }

    public void start() {
        obtainNewParams();
        paramsListenerHook = startParamsPollingScheduler();
        whisperExecutorHook = startWhisperingScheduler(false);
    }

    public void stop() {
        if (whisperExecutorHook != null) {
            whisperExecutorHook.cancel(false);
        }
        if (paramsListenerHook != null) {
            paramsListenerHook.cancel(false);
        }
    }

    private void obtainNewParams() {
        try {
            Optional<WhisperingParams> newParams = serviceConnector.obtainParams(lastKnownParams);
            if (newParams.isPresent()) {
                long oldWhisperingInterval = lastKnownParams != null ? lastKnownParams.getWhisperingIntervalMillis() : -1;

                WhisperingParams params = newParams.get();
                paramsService.save(params);
                setLastKnownParams(params);

                if (whisperExecutorHook != null && oldWhisperingInterval != params.getWhisperingIntervalMillis()) {
                    whisperExecutorHook.cancel(false);
                    whisperExecutorHook = startWhisperingScheduler(true);
                }
            }
        } catch (ServiceException e) {
            e.printStackTrace(System.err);
        }
    }

    private ScheduledFuture startParamsPollingScheduler() {
        ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor();
        return scheduledExecutor.scheduleWithFixedDelay(this::obtainNewParams, lastKnownParams.getParamsPollingIntervalMillis(), lastKnownParams.getParamsPollingIntervalMillis(), TimeUnit.MILLISECONDS);
    }

    private ScheduledFuture startWhisperingScheduler(boolean setInitialDelay) {
        ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor();
        long delay = setInitialDelay ? lastKnownParams.getWhisperingIntervalMillis() : 0;
        return scheduledExecutor.scheduleWithFixedDelay(this::whisper, delay, lastKnownParams.getWhisperingIntervalMillis(), TimeUnit.MILLISECONDS);
    }

    private void whisper() {
        List<Process> processList = commandService.getProcessList();
        List<Process> appropriate = filterByPattern(processList, lastKnownAppropriatePattern);
        List<Process> inappropriate = filterByPattern(processList, lastKnownInappropriatePattern);

        try {
            serviceConnector.sendProcesses(new ProcessSnapshot(ZonedDateTime.now(), appropriate, inappropriate));
        } catch (ServiceException e) {
            e.printStackTrace(System.err);
        }
    }

    private List<Process> filterByPattern(List<Process> processList, Predicate<String> pattern) {
        return processList.stream()
                .filter(process -> pattern.test(process.getName()))
                .collect(Collectors.toList());
    }

    private void setLastKnownParams(WhisperingParams params) {
        lastKnownParams = params;
        lastKnownAppropriatePattern = createProcessPattern(params.getAppropriatePatterns());
        lastKnownInappropriatePattern = createProcessPattern(params.getInappropriatePatterns());
    }

    private Predicate<String> createProcessPattern(Set<String> patterns) {
        String patternStr = String.format(PROCESS_PATTERN_TEMPLATE, String.join("|", patterns));
        return Pattern.compile(patternStr).asPredicate();
    }
}
